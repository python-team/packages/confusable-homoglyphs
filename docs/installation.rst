============
Installation
============

If available, install an appropriate package from your distribution:

.. image:: https://repology.org/badge/vertical-allrepos/confusable-homoglyphs.svg
   :target: https://repology.org/project/confusable-homoglyphs/versions

Otherwise you can install from PyPi:

at the command line::

    $ easy_install confusable_homoglyphs

or, if you have virtualenvwrapper installed::

    $ mkvirtualenv confusable_homoglyphs
    $ pip install confusable_homoglyphs
